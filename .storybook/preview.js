import {muiTheme} from 'storybook-addon-material-ui';
import {muiTheme as defaultTheme} from './../src/styles/muiTheme';
export const decorators = [muiTheme([defaultTheme])];

export const parameters = {
  actions: {argTypesRegex: '^on[A-Z].*'},
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};
