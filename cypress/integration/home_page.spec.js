/// <reference types="cypress" />

describe('Test Home Page - load', () => {
  beforeEach(() => {
    cy.visit(Cypress.env('homePage_url'));
  });
  it('should be contain Search Input', () => {
    cy.get('[data-cy=searchInput]').should('be.visible');
  });
  it('should be contain Amount Input', () => {
    cy.get('[data-cy=AmountInput]').should('be.visible');
  });
});

describe('Test Home Page - Country Search', () => {
  before(() => {
    cy.intercept('GET', '**/restcountries.com/**').as('getCountriesRequest');
    cy.intercept('GET', '**/v6.exchangerate-api.com/**').as(
      'getExchangeRateRequest',
    );
    cy.visit(Cypress.env('homePage_url'));
    cy.wait('@getCountriesRequest');
    cy.wait('@getExchangeRateRequest');
  });
  it('should load countries list', () => {
    cy.get('[data-cy=countriesList] [data-cy=countryItem]').should(
      'be.visible',
    );
  });
  it('should get country list when seach input changes', () => {
    cy.intercept('GET', '**/restcountries.com/**').as('getCountriesRequest');
    cy.get('[data-cy=searchInput]').type('Am');
    cy.wait('@getCountriesRequest')
      .its('response.statusCode')
      .should('eq', 200);
  });

  it('should show not found box when request fails', () => {
    cy.intercept('GET', '**/restcountries.com/**').as('getCountriesRequest');
    cy.get('[data-cy=searchInput]').type('Abcdef');
    cy.wait('@getCountriesRequest')
      .its('response.statusCode')
      .should('eq', 404);
    cy.get('[data-cy=notFound]').should('be.visible');
  });
});

describe('Test Home Page - Bookmarks', () => {
  before(() => {
    cy.intercept('GET', '**/restcountries.com/**').as('getCountriesRequest');
    cy.intercept('GET', '**/v6.exchangerate-api.com/**').as(
      'getExchangeRateRequest',
    );
    cy.visit(Cypress.env('homePage_url'));
    cy.wait('@getCountriesRequest');
    cy.wait('@getExchangeRateRequest');
  });
  it('should save bookmark on click on bookmark button', () => {
    cy.get('[data-cy=bookmarkButton]')
      .first()
      .click()
      .then(() => {
        cy.get('[data-cy=bookmarksList] [data-cy=countryItem]').should(
          'be.visible',
        );
      });
  });
});

describe('Test Home Page - Amount', () => {
  before(() => {
    cy.intercept('GET', '**/restcountries.com/**').as('getCountriesRequest');
    cy.intercept('GET', '**/v6.exchangerate-api.com/**').as(
      'getExchangeRateRequest',
    );
    cy.visit(Cypress.env('homePage_url'));
    cy.wait('@getCountriesRequest');
    cy.wait('@getExchangeRateRequest');
  });
  it('should update currency on amount change', () => {
    let currencyAmount;
    let newCurrencyAmount;
    cy.get('[data-cy=currencyAmount]').then($element => {
      currencyAmount = $element.first().text();
      currencyAmount = currencyAmount.replace(/\D/g, '');
    });
    cy.get('[data-cy=AmountInput]').type(2);
    cy.wait(1500);
    cy.get('[data-cy=currencyAmount]').then($element => {
      newCurrencyAmount = $element.first().text();
      newCurrencyAmount = newCurrencyAmount.replace(/\D/g, '');
      expect(newCurrencyAmount / currencyAmount).eq(12);
    });
  });
});
