# AnyFin Company Test

The test task for Quin Company used React-js, TypeScript and styled-components(Emotion).

## Url

https://quin-emadecode.vercel.app/

## Sections

### [Available Scripts](#available-scripts-1)

### [Documentation](#documentation-1)

&nbsp;
&nbsp;

---

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Runs the tests.

### `npm run storybook`

Runs the Storybook to view documents and components in isolation.
Open [http://localhost:6006](http://localhost:6006) to view it in the browser.

### `npm cy`

Opens the test runner in the visual mode.

### `npm cy`

Opens the test runner in the visual and production mode.

### `npm cy:test`

Launches the test runner in the interactive watch mode.

### `npm cy:test:prod`

Launches the test runner in the interactive watch and production mode.

### `npm cy:gr`

Generates test Results in a visual mode.(HTML)
**Note: Don't forget to run `npm cy:test` before that**

&nbsp;
&nbsp;

---

# Documentation

&nbsp;

### First of all,

# Yaaaay! I did it.

&nbsp;

> I know some of the things I did for such a small app would be over-engineering, but as you can imagine, I tried to develop this app with the idea of enterprise applications.

&nbsp;

## Why I have used React-Query?

React query has a great built-in method for Caching and Memoization API responses. And it’s easy to config for mocking data on the test stage.
I’ve created a generic function for fetching data with react-query. So, in this way we don’t need to repeat fetch function on every component.

## Why I have used Redux?

I could use Context-Api, but I prefer to use Redux State Management, That’s because we have a cleaner way for data flow (flux) and also we can track state change more easily with that flow. And it is easier to test.

## Why I have used MUI?

I had a limited time for this task and I didn’t want to spend much time on styling. But in a big-scale project with a suitable time, I prefer to implement a design system or use Tailwind. That’s because some implementation (like the mobile-first approach) is better with Tailwind.

## What was my approach to testing?

I’ve used Cypress for E2E testing and react testing library for unit and integration tests.
You can find a custom render function for react testing library on “Core” directory. The reason I created a custom render method was to add required providers for a component that is going to be tested. In this way, if we add a new provider to our top-level component in the future, we can easily change the custom render method and all tests would be edited.

## Why I have used Storybook?

The storybook library gives you the ability to see each component separately in an isolation mode. And you can also test different use cases of the components.
And last but not least, A CI/CD Tool:
I’ve used Gitlab.com and Vercel.com to implement continuous integration and continuous development. So u can see an online version of the app and we can also make changes on part of the app in the future if you want.
