import {css} from '@emotion/react';

export const defaultFont = css`
  @font-face {
    font-family: Roboto;
    src: url('../fonts/roboto/Roboto-Thin.ttf') format('truetype');
    font-weight: thin;
    font-style: normal;
  }

  @font-face {
    font-family: Roboto;
    src: url('../fonts/roboto/Roboto-Regular.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: Roboto;
    src: url('../fonts/roboto/Roboto-Medium.ttf') format('truetype');
    font-weight: medium;
    font-style: normal;
  }

  @font-face {
    font-family: Roboto;
    src: url('../fonts/roboto/Roboto-Bold.ttf') format('truetype');
    font-weight: bold;
    font-style: normal;
  }
`;
