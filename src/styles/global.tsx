import {FC} from 'react';
import {Global, css} from '@emotion/react';
import {defaultFont} from './fonts';
import {Theme} from '@mui/material';

export type IPropsModel = {
  theme: Theme;
};

const GlobalStyle: FC<IPropsModel> = ({theme}) => {
  return (
    <>
      <Global styles={defaultFont} />
      <Global
        styles={css`
          * {
            font-family: ${theme.typography.fontFamily};
          }
          html {
            font-size: 62.5%;
            @media only screen and (min-width: 728px) {
              font-size: 87.5%;
            }
          }
          body {
            font-family: ${theme.typography.fontFamily};
            background-color: ${theme.palette.background.default};
            direction: ${theme.direction};
          }
          ::-webkit-scrollbar {
            width: 0.6rem;
            height: 0.5rem;
          }

          /* Track */
          ::-webkit-scrollbar-track {
            background: #f1f1f1;
          }

          /* Handle */
          ::-webkit-scrollbar-thumb {
            background-color: #a8bbbf;
            border-radius: 3rem;
          }

          /* Handle on hover */
          ::-webkit-scrollbar-thumb:hover {
            background-color: #9eb2b7;
          }
        `}
      />
    </>
  );
};

export default GlobalStyle;
