import {lightThemeColors as defaultColors} from './colors';
import {createTheme, Direction, Theme} from '@mui/material/styles';
import darkScrollbar from '@mui/material/darkScrollbar';
import {PaletteMode} from '@mui/material';

export const createThemeHandler = (
  paletteMode: PaletteMode,
  direction: Direction,
): Theme => {
  const fontFamily = 'Roboto';

  const createdTheme = createTheme({
    direction: direction,
    spacing: (factor: number) => `${0.25 * factor}rem`, // (Bootstrap strategy)
    components: {
      MuiCssBaseline: {
        styleOverrides: {
          body: paletteMode === 'dark' ? darkScrollbar() : null,
        },
      },
    },
    typography: {
      htmlFontSize: 10,
      fontFamily: [fontFamily, '"Helvetica Neue"', 'Arial', 'sans-serif'].join(
        ',',
      ),
      fontWeightLight: 200,
      fontWeightRegular: 300,
      fontWeightMedium: 400,
      fontWeightBold: 600,
    },
    palette: {
      mode: paletteMode,
      ...defaultColors,
    },
  });

  createdTheme.typography.button = {
    fontFamily: fontFamily,
    fontSize: '1.1rem',
    fontWeight: 300,
  };

  createdTheme.typography.caption = {
    fontFamily: fontFamily,
    fontSize: '1.1rem',
  };

  createdTheme.typography.h3 = {
    fontFamily: fontFamily,
    fontSize: '1.2rem',
  };

  createdTheme.typography.h4 = {
    fontFamily: fontFamily,
    fontSize: '1.2rem',
    margin: 0,
  };

  createdTheme.typography.body1 = {
    fontFamily: fontFamily,
    fontSize: '1.2rem',
  };

  createdTheme.typography.body2 = {
    fontFamily: fontFamily,
    fontSize: '1.2rem',
  };

  return createdTheme;
};

export const muiTheme: Theme = createThemeHandler('light', 'ltr');
export const muiThemeDark: Theme = createThemeHandler('dark', 'ltr');

export interface IStyledThemeProps {
  theme?: Theme;
}
