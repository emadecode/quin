import React from 'react';
import './App.css';
import {
  Container,
  CssBaseline,
  ThemeProvider as MUIThemeProvider,
} from '@mui/material';
import {Provider as ReduxProvider} from 'react-redux';
import {QueryClient, QueryClientProvider} from 'react-query';
import {muiTheme} from './styles/muiTheme';
import {store} from './redux/store';
import GlobalStyle from './styles/global';
import MoonShot from './presentation/MoonShot';
import GeneralLoading from './shared/general-loading/GeneralLoading';

function App() {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <ReduxProvider store={store}>
        <MUIThemeProvider theme={muiTheme}>
          <CssBaseline />
          <GlobalStyle theme={muiTheme} />

          <Container sx={{py: '1rem', justifyContent: 'center'}}>
            <GeneralLoading />
            <MoonShot />
          </Container>
        </MUIThemeProvider>
      </ReduxProvider>
    </QueryClientProvider>
  );
}

export default App;
