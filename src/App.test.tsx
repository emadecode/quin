import React from 'react';
import {render, screen} from '@testing-library/react';
import App from './App';

describe('Test App component', () => {
  it('should show general loading on fetchig data', async () => {
    render(<App />);
    const loadingEl: HTMLElement = await screen.findByTestId('general-loading');

    expect(loadingEl).toBeInTheDocument();
  });
});
