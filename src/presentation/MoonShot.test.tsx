import React from 'react';
import {screen} from '@testing-library/react';
import MoonShot from './MoonShot';
import {render} from '../core/testProviders';

describe('Test MoonShot Component', () => {
  it('should load filter button', () => {
    render(<MoonShot />);
    const filterButtonEl: HTMLElement =
      screen.getByTestId('open-dialog-button');

    expect(filterButtonEl).toBeInTheDocument();
  });
  it('should load map loading', () => {
    render(<MoonShot />);
    const mapLoadingEl: HTMLElement = screen.getByTestId('map-loading');

    expect(mapLoadingEl).toBeInTheDocument();
  });

  it('should load map after receiving data', async () => {
    render(<MoonShot />);
    const mapContainerEl: HTMLElement = await screen.findByTestId(
      'map-container',
    );

    expect(mapContainerEl).toBeInTheDocument();
  });
});
