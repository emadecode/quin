import {Dialog, DialogTitle, DialogContent, Typography} from '@mui/material';
import {FC, useState, useEffect} from 'react';
import {Marker, Tooltip, useMap} from 'react-leaflet';
import {useSelector} from 'react-redux';
import {RootState} from '../../redux/ducks/reducer';
import AgencyItem from '../agency-item/AgencyItem';
import styles from './MarkersList.module.scss';

type MarkerPositionModel = [number, number];

interface IMarkersListProps {
  data: Array<any>;
}

const MarkersList: FC<IMarkersListProps> = ({data}) => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [currentItem, setCurrentItem] = useState<any>(null);
  const map = useMap();

  useEffect(() => {
    let mapMarklers: Array<MarkerPositionModel> = [];
    data.forEach(item => {
      const {pad} = item;
      mapMarklers = [
        ...mapMarklers,
        [parseFloat(pad.latitude), parseFloat(pad.longitude)],
      ];
    });
    if (data.length) {
      map.fitBounds(mapMarklers);
    }
  });
  return (
    <>
      {data.map(item => {
        const {pad} = item;
        return (
          <Marker
            position={[parseFloat(pad.latitude), parseFloat(pad.longitude)]}
            key={item.id}
            eventHandlers={{
              click: e => {
                setCurrentItem(item);
                setIsModalOpen(true);
              },
            }}
            data-testid={'marker'}
          >
            <Tooltip>{item.name}</Tooltip>
          </Marker>
        );
      })}
      {currentItem && (
        <Dialog
          open={isModalOpen}
          onClose={() => setIsModalOpen(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <DialogTitle>{currentItem.name}</DialogTitle>
          <DialogContent>
            <div className={styles.markerInfo}>
              <Typography variant="h4" className={styles.markerInfo__title}>
                Time of the Launch
              </Typography>
              <Typography variant="h4" className={styles.markerInfo__value}>
                {new Date(currentItem.window_start).toUTCString()}
              </Typography>
            </div>
            <div className={styles.markerInfo}>
              <Typography variant="h4" className={styles.markerInfo__title}>
                Pad name
              </Typography>
              <Typography variant="h4" className={styles.markerInfo__value}>
                {currentItem.pad.name}
              </Typography>
            </div>
            {currentItem.pad.agency_id && (
              <div className={styles.markerInfo}>
                <Typography variant="h4" className={styles.markerInfo__title}>
                  Agency
                </Typography>
                <Typography variant="h4" className={styles.markerInfo__value}>
                  <AgencyItem agencyId={currentItem.pad.agency_id} />
                </Typography>
              </div>
            )}
          </DialogContent>
        </Dialog>
      )}
    </>
  );
};

export default MarkersList;
