export interface IFilterDataModel {
  window_start__gt?: string | null;
  window_start__lt?: string | null;
  window_end__lt?: string | null;
}
