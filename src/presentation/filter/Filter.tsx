import React, {useState} from 'react';
import {Button, Dialog, DialogContent, TextField} from '@mui/material';
import styles from './Filter.module.scss';
import {DesktopDatePicker, LocalizationProvider} from '@mui/x-date-pickers';
import {AdapterMoment} from '@mui/x-date-pickers/AdapterMoment';
import {useDispatch} from 'react-redux';
import {addFilterData} from './../../redux/ducks/filter';
import {covertDateToISO} from '../../core/utils';

const Filter = () => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

  const [startDate, setStartDate] = React.useState<Date | null>(null);
  const [EndData, setEndDate] = React.useState<Date | null>(null);

  const dispatch = useDispatch();
  const onSubmit = (event: any) => {
    event.preventDefault();
    const {elements} = event.target;
    dispatch(
      addFilterData({
        window_start__gt: covertDateToISO(elements.startDate.value),
        window_end__lt: covertDateToISO(elements.endDate.value),
      }),
    );
    setIsModalOpen(false);
  };

  return (
    <div className={styles.filterSection}>
      <Button
        variant="contained"
        color="info"
        size="medium"
        onClick={() => setIsModalOpen(true)}
        data-testid="open-dialog-button"
      >
        Filter
      </Button>
      <Dialog
        open={isModalOpen}
        onClose={() => setIsModalOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        data-testid="filter-dialog"
      >
        <DialogContent>
          <form onSubmit={onSubmit}>
            <LocalizationProvider dateAdapter={AdapterMoment}>
              <div className={styles.filterItem}>
                <DesktopDatePicker
                  label="Date desktop"
                  inputFormat="MM/DD/YYYY"
                  value={startDate}
                  onChange={value => setStartDate(value)}
                  renderInput={params => (
                    <TextField {...params} name="startDate" />
                  )}
                />
              </div>
              <div className={styles.filterItem}>
                <DesktopDatePicker
                  label="Date desktop"
                  inputFormat="MM/DD/YYYY"
                  value={EndData}
                  onChange={value => setEndDate(value)}
                  renderInput={params => (
                    <TextField {...params} name="endDate" />
                  )}
                />
              </div>
            </LocalizationProvider>

            <Button
              variant="contained"
              color="info"
              fullWidth={true}
              onClick={() => setIsModalOpen(true)}
              type="submit"
              data-testid="filter-form-submit-button"
            >
              submit
            </Button>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default Filter;
