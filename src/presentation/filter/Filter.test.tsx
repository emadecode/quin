import React from 'react';
import {screen, fireEvent} from '@testing-library/react';
import Filter from './Filter';
import {render} from '../../core/testProviders';

describe('Test Filter Component', () => {
  it('should Open Dialog on click', () => {
    render(<Filter />);
    const openDialogButtonEl: HTMLElement =
      screen.getByTestId('open-dialog-button');
    fireEvent.click(openDialogButtonEl);

    const filterDialogEl: HTMLElement = screen.getByTestId('filter-dialog');

    expect(filterDialogEl).toBeInTheDocument();
  });

  it('should filter results', async () => {
    render(<Filter />);
    const openDialogButtonEl: HTMLElement =
      screen.getByTestId('open-dialog-button');
    fireEvent.click(openDialogButtonEl);

    const submitButtonEl: HTMLElement = screen.getByTestId(
      'filter-form-submit-button',
    );

    expect(submitButtonEl).toBeInTheDocument();
  });
});
