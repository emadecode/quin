export interface ILaunchItemModel {
  id: string;
  last_updated: string;
  net: string;
  window_end: string;
  window_start: string;
  name: string;
  pad: {
    agency_id?: number;
    name: string;
    latitude: string;
    longitude: string;
  };
}
