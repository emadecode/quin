import {CircularProgress} from '@mui/material';
import React, {FC, useState} from 'react';
import {useQuery} from 'react-query';
import {CreateQuery} from '../../core/queryFunctions';
import {IAgencyItemModel} from './models/AgencyItem.models';

interface IPropsModel {
  agencyId: number;
}

const AgencyItem: FC<IPropsModel> = ({agencyId}) => {
  // const [agencyData] = useState<any>({
  //   id: 166,
  //   url: 'https://lldev.thespacedevs.com/2.2.0/agencies/166/',
  //   name: 'US Navy',
  //   featured: false,
  //   type: 'Government',
  //   country_code: 'USA',
  //   abbrev: 'USN',
  //   description:
  //     'The Navy was competed with the Army to put the first American satellite into orbit. Their first two Vanguard missions failed, with the second being a highly publicized failure, as it exploded on the launchpad. They went on to put 3 satellites into orbit but did not beat the Army to be the first. Their work in space continues today largely on military satellites.',
  //   administrator: null,
  //   founding_year: '1963',
  //   launchers: 'None',
  //   spacecraft: 'Vanguard 1-3 | Parker Solar Probe',
  //   parent: null,
  //   launch_library_url: 'https://launchlibrary.net/1.4/agency/166',
  //   total_launch_count: 6,
  //   successful_launches: 0,
  //   consecutive_successful_launches: 0,
  //   failed_launches: 6,
  //   pending_launches: 0,
  //   successful_landings: 0,
  //   failed_landings: 0,
  //   attempted_landings: 0,
  //   consecutive_successful_landings: 0,
  //   info_url: 'http://www.navy.mil',
  //   wiki_url: 'http://en.wikipedia.org/wiki/US_Navy',
  //   logo_url: null,
  //   image_url: null,
  //   nation_url: null,
  //   launcher_list: [
  //     {
  //       id: 344,
  //       url: 'https://lldev.thespacedevs.com/2.2.0/config/launcher/344/',
  //       name: 'Project Pilot',
  //       description:
  //         "The NOTS-EV-1 Pilot, also known as NOTSNIK was an expendable launch system and anti-satellite weapon developed by the United States Navy's United States Naval Ordnance Test Station (NOTS).",
  //       family: '',
  //       full_name: 'Project Pilot',
  //       variant: '',
  //       alias: '',
  //       min_stage: 5,
  //       max_stage: 5,
  //       length: 4.4,
  //       diameter: 0.76,
  //       maiden_flight: '1958-07-04',
  //       launch_mass: 0,
  //       leo_capacity: 1,
  //       gto_capacity: null,
  //       to_thrust: 125,
  //       apogee: null,
  //       vehicle_range: null,
  //       image_url: null,
  //       info_url: '',
  //       wiki_url: 'https://en.wikipedia.org/wiki/NOTS-EV-1_Pilot',
  //       consecutive_successful_launches: 0,
  //       successful_launches: 0,
  //       failed_launches: 6,
  //       pending_launches: 0,
  //     },
  //   ],
  //   spacecraft_list: [],
  // });
  const {data, isLoading} = useQuery(
    CreateQuery<IAgencyItemModel>(
      `https://lldev.thespacedevs.com/2.2.0/agencies/${agencyId}`,
    ),
  );

  if (isLoading) {
    return <CircularProgress />;
  }

  return <>{data?.name}</>;
};

export default AgencyItem;
