import React, {useEffect, useState} from 'react';
import {useQuery} from 'react-query';
import {useDispatch, useSelector} from 'react-redux';
import {CreateQuery} from '../core/queryFunctions';
import {ILaunchItemModel} from './models/MoonShot.Models';
import {addLaunchList} from './../redux/ducks/launch-list';
import LaunchMap from './map/LaunchMap';
import Filter from './filter/Filter';
import {RootState} from '../redux/ducks/reducer';
import {CircularProgress} from '@mui/material';

const MoonShot = () => {
  const dipatch = useDispatch();
  const filterData = useSelector((s: RootState) => s.filter.filter);
  const [queryString, setQueryString] = useState<string>('');

  useEffect(() => {
    let newQueryString = Object.entries(filterData)
      .map(([key, value]) => {
        return value ? key + '=' + value : null;
      })
      .join('&');
    setQueryString(newQueryString);
  }, [filterData]);

  const {isLoading} = useQuery(
    CreateQuery<{results: Array<ILaunchItemModel>}>(
      `https://lldev.thespacedevs.com/2.2.0/launch/?${queryString}`,
      {
        onSuccess: data => {
          dipatch(addLaunchList(data.results));
        },
      },
    ),
  );

  return (
    <>
      <Filter />
      {isLoading ? (
        <CircularProgress data-testid="map-loading" />
      ) : (
        <LaunchMap />
      )}
    </>
  );
};

export default MoonShot;
