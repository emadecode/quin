import React, {FC, useEffect, useState} from 'react';
import {MapContainer, TileLayer} from 'react-leaflet';
import styles from './LaunchMap.module.scss';
import 'leaflet/dist/leaflet.css';
import MarkersList from './../markers-list/MarkersList';
import {useSelector} from 'react-redux';
import {RootState} from '../../redux/ducks/reducer';

const LaunchMap = () => {
  const launchList = useSelector((s: RootState) => s.launchList.launchList);

  return (
    <div data-testid="map-container">
      <MapContainer className={styles.leafletContainer} zoom={13}>
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        <MarkersList data={launchList} />
      </MapContainer>
    </div>
  );
};

export default LaunchMap;
