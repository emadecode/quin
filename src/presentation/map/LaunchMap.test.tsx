import React from 'react';
import {screen} from '@testing-library/react';
import LaunchMap from './LaunchMap';
import {render} from '../../core/testProviders';

describe('Test LaunchMap Component', () => {
  it('should load map container', () => {
    render(<LaunchMap />);
    const mapContainerEl: HTMLElement = screen.getByTestId('map-container');

    expect(mapContainerEl).toBeInTheDocument();
  });
});
