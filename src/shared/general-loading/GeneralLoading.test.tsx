import React from 'react';
import {screen} from '@testing-library/react';
import GeneralLoading from './GeneralLoading';
import {render} from '../../core/testProviders';
import {setLoadingStatus} from '../../redux/ducks/general';
import {store} from '../../redux/store';

describe('Test GeneralLoading component', () => {
  it('should show when is true', async () => {
    render(<GeneralLoading />);
    store.dispatch(setLoadingStatus(true));
    const loadingEl: HTMLElement = await screen.findByTestId('general-loading');

    expect(loadingEl).toBeInTheDocument();
  });
});
