import React from 'react';
import {useSelector} from 'react-redux';
import {RootState} from '../../redux/ducks/reducer';
import {LinearProgress} from '@mui/material';

const GeneralLoading = () => {
  const appIsLoading = useSelector(
    (state: RootState) => state.general.isLoading,
  );
  return (
    <>
      {appIsLoading && (
        <LinearProgress
          data-testid="general-loading"
          sx={{position: 'fixed', top: 0, right: 0, left: 0}}
        />
      )}
    </>
  );
};

export default GeneralLoading;
