import React, {FC, ReactElement} from 'react';
import {render, RenderOptions} from '@testing-library/react';
import {Provider as ReduxProvider} from 'react-redux';
import {CssBaseline, ThemeProvider as MUIThemeProvider} from '@mui/material';
import {store} from '../redux/store';
import {muiTheme} from '../styles/muiTheme';
import GlobalStyle from '../styles/global';
import {QueryClient, QueryClientProvider} from 'react-query';

interface IpropsModel {
  children: ReactElement;
}

const AllTheProviders: FC<IpropsModel> = ({children}) => {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <ReduxProvider store={store}>
        <MUIThemeProvider theme={muiTheme}>
          <CssBaseline />
          <GlobalStyle theme={muiTheme} />
          {children}
        </MUIThemeProvider>
      </ReduxProvider>
    </QueryClientProvider>
  );
};

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, 'wrapper'>,
) => render(ui, {wrapper: AllTheProviders, ...options});

export * from '@testing-library/react';
export {customRender as render};
