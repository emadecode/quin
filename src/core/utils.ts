export const covertDateToISO = (date: string): string | null => {
  return date ? new Date(date).toISOString() : null;
};
