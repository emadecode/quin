import {rest} from 'msw';
import {setupServer} from 'msw/node';

const server = setupServer(
  rest.get(
    'https://lldev.thespacedevs.com/2.2.0/agencies/*',
    (req, res, ctx) => {
      return res(
        ctx.status(200),
        ctx.json({
          id: 166,
          name: 'US Navy',
        }),
      );
    },
  ),
  rest.get('https://lldev.thespacedevs.com/2.2.0/launch/*', (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        results: [
          {
            id: 'e3df2ecd-c239-472f-95e4-2b89b4f75800',
            name: 'Sputnik 8K74PS | Sputnik 1',
            last_updated: '2021-08-31T20:43:53Z',
            net: '1957-10-04T19:28:34Z',
            window_end: '1957-10-04T19:28:34Z',
            window_start: '1957-10-04T19:28:34Z',
            pad: {
              id: 32,
              name: '1/5',
              latitude: '45.92',
              longitude: '63.342',
            },
          },
        ],
      }),
    );
  }),
  rest.get('*', (req, res, ctx) => {
    console.error(`Please add request handler for ${req.url.toString()}`);
    return res(
      ctx.status(500),
      ctx.json({error: 'Please add request handler'}),
    );
  }),
);

beforeAll(() => server.listen());
beforeEach(() => server.resetHandlers());
afterAll(() => server.close());

export {server, rest};
