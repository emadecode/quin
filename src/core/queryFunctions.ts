import {UseQueryOptions} from 'react-query';
import axios from './axios';

export const CreateQuery = <T>(
  key: string,
  config: UseQueryOptions<T> = {},
) => ({
  queryKey: ['CreateQuery', key],
  queryFn: () => axios.get<T>(key).then(res => res.data),
  ...config,
});
