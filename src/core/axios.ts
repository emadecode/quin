import axiosApi from 'axios';
import {setLoadingStatus} from '../redux/ducks/general';
import {store} from '../redux/store';

// ** Used for canceling a request **
export const cancelToken = axiosApi.CancelToken;
export const isCancel = axiosApi.isCancel;

// ** Creating instance of axios **
const axios = axiosApi.create({
  baseURL: process.env.REACT_APP_BASE_API,
  headers: {
    'Content-Type': 'application/json',
  },
  timeout: 5000,
});

axios.interceptors.request.use(
  config => {
    config.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json; charset=utf8',
    };
    store.dispatch(setLoadingStatus(true));
    return config;
  },
  error => {
    store.dispatch(setLoadingStatus(false));
    Promise.reject(error);
  },
);

axios.interceptors.response.use(
  response => {
    store.dispatch(setLoadingStatus(false));
    return response;
  },
  async error => {
    store.dispatch(setLoadingStatus(false));
    return Promise.reject(error);
  },
);

export default axios;
