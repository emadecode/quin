import {createAction, createReducer} from '@reduxjs/toolkit';

const SET_LOADING_STATUS = 'quin/general/SET_LOADING_STATUS';

export const setLoadingStatus = createAction<boolean>(SET_LOADING_STATUS);

interface IStateModel {
  isLoading: boolean;
}
const initialState: IStateModel = {
  isLoading: false,
};

export default createReducer<IStateModel>(initialState, builder => {
  builder.addCase(setLoadingStatus, (state, action) => {
    state.isLoading = action.payload;
  });
});
