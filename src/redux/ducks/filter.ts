import {createAction, createReducer} from '@reduxjs/toolkit';
import {IFilterDataModel} from '../../presentation/filter/models/Filter.Models';
const initDate = new Date();
const ADD_FILTER_DATA = 'quin/filter/ADD_FILTER_DATA';

export const addFilterData = createAction<IFilterDataModel>(ADD_FILTER_DATA);

interface IStateModel {
  filter: IFilterDataModel;
}
const initialState: IStateModel = {
  filter: {
    window_start__lt: new Date(initDate.setMonth(initDate.getMonth() + 3))
      .toISOString()
      .toString(),
  },
};

export default createReducer<IStateModel>(initialState, builder => {
  builder.addCase(addFilterData, (state, action) => {
    state.filter = action.payload;
  });
});
