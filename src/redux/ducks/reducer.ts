import {Action, combineReducers, Reducer} from '@reduxjs/toolkit';

import launchList from './launch-list';
import filter from './filter';
import general from './general';

const appReducer = combineReducers({
  launchList,
  filter,
  general,
});

const rootReducer: Reducer = (state: RootState, action: Action) => {
  return appReducer(state, action);
};

export type RootState = ReturnType<typeof appReducer>;

export default rootReducer;
