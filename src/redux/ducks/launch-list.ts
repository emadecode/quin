import {createAction, createReducer} from '@reduxjs/toolkit';
import {ILaunchItemModel} from './../../presentation/models/MoonShot.Models';

const ADD_LAUNCH_LIST = 'quin/launchList/ADD_LAUNCH_LIST';

export const addLaunchList =
  createAction<Array<ILaunchItemModel>>(ADD_LAUNCH_LIST);

interface IStateModel {
  launchList: Array<ILaunchItemModel>;
}
const initialState: IStateModel = {
  launchList: [],
};

export default createReducer<IStateModel>(initialState, builder => {
  builder.addCase(addLaunchList, (state, action) => {
    state.launchList = action.payload;
  });
});
