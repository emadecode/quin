import {configureStore} from '@reduxjs/toolkit';
import reducer from './ducks/reducer';

export const store = configureStore({
  reducer: reducer,
  devTools: true,
  middleware: getDefaultMiddleware => getDefaultMiddleware(),
});

// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
